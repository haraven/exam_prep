package exam.common.service;

import exam.common.model.Person;

import java.util.List;

public interface PersonService {
    Person searchPersonBySSN(String ssn);
}
