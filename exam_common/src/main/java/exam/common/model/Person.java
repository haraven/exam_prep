package exam.common.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
public class Person implements Serializable {
    private Long id;
    private String ssn;
    private String name;

    public Person(String ssn, String name) {
        this.ssn = ssn;
        this.name = name;
    }
}
