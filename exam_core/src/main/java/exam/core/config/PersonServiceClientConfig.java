package exam.core.config;

import exam.common.service.PersonService;
import exam.core.service.PersonServiceClientImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianProxyFactoryBean;

@Configuration
public class PersonServiceClientConfig {
    @Bean
    public HessianProxyFactoryBean personService() {
        HessianProxyFactoryBean factory = new HessianProxyFactoryBean();
        factory.setServiceUrl("http://localhost:13338/hessian/books");
        factory.setServiceInterface(PersonService.class);
        return factory;
    }

    @Bean
    public PersonServiceClientImpl personServiceClient() { return new PersonServiceClientImpl(); }
}
