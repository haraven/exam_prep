package exam.core.repository;

import exam.core.model.Author;

public interface AuthorRepository extends BaseRepository<Long, Author> {
}
