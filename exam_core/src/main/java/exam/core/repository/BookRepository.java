package exam.core.repository;

import exam.core.model.Book;

public interface BookRepository extends BaseRepository<Long, Book> {}
