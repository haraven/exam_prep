package exam.core.repository;

import exam.core.model.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Transactional
@NoRepositoryBean
public interface BaseRepository<ID extends Serializable, T extends BaseEntity<ID>> extends JpaRepository<T, ID> {}
