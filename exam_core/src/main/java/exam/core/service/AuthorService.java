package exam.core.service;

import exam.common.model.Person;
import exam.core.model.Author;
import exam.core.model.Book;

import java.util.List;

public interface AuthorService {
    List<Author> getAll();
    Author getOne(Long author_id);
    Author addAuthor(Author author);
    List<Book> getAuthorBooks(Long author_id);
}
