package exam.core.service;

import exam.common.model.Person;
import exam.core.model.Author;
import exam.core.model.Book;
import exam.core.repository.AuthorRepository;
import exam.core.service.exceptions.ServiceHibernateException;
import net.sf.ehcache.hibernate.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {
    @Autowired
    private AuthorRepository author_repo;

    @Override
    public List<Author> getAll() {
        try {
            List<Author> authors = author_repo.findAll();
            authors.forEach(author -> Hibernate.initialize(author.getBooks()));

            return authors;
        } catch(Exception e) {
            throw new ServiceHibernateException("Error while retrieving authors", e);
        }
    }

    @Override
    public Author getOne(Long author_id) {
        try {
            Author author = author_repo.findOne(author_id);
            Hibernate.initialize(author.getBooks());

            return author;
        } catch (Exception e) {
            throw new ServiceHibernateException("Error while retrieving single author", e);
        }
    }

    @Override
    public Author addAuthor(Author author) {
        author_repo.save(author);

        return author;
    }

    @Override
    public List<Book> getAuthorBooks(Long author_id) {
        Author author = author_repo.findOne(author_id);
        Hibernate.initialize(author.getBooks());

        return author.getBooks();
    }
}
