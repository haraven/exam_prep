package exam.core.service;

import exam.core.model.Book;
import exam.core.repository.BookRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.jpa.HibernateEntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class BookServiceImpl implements BookService {
    @PersistenceContext
    private EntityManager entity_manager;

    @Autowired
    private BookRepository book_repo;

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> getAuthorBooks(Long author_id) {
        HibernateEntityManager manager = entity_manager.unwrap(HibernateEntityManager.class);
        Session session = manager.getSession();

        String sql = "SELECT * FROM Book B WHERE B.AuthorID = :author_id";

        Query select = session.createSQLQuery(sql)
                .addEntity("B", Book.class)
                .setLong("author_id", author_id);

        return select.list();
    }
}
