package exam.core.service;

import exam.core.model.Book;

import java.util.List;

public interface BookService {
    List<Book> getAuthorBooks(Long author_id);
}
