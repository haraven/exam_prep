package exam.core.service.exceptions;

public class ServiceRepoException extends RuntimeException {
    public ServiceRepoException(String message) {
        super(message);
    }

    public ServiceRepoException(Exception e)
    {
        super(e);
    }

    public ServiceRepoException(String message, Throwable cause) {
        super(message, cause);
    }
}
