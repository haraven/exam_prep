package exam.core.service.exceptions;

public class ServiceHibernateException extends RuntimeException{
    public ServiceHibernateException(String message) {
        super(message);
    }

    public ServiceHibernateException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceHibernateException(Throwable cause) {
        super(cause);
    }
}
