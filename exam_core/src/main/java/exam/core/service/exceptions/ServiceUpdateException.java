package exam.core.service.exceptions;

public class ServiceUpdateException extends RuntimeException {
    public ServiceUpdateException(String message) {
        super(message);
    }

    public ServiceUpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceUpdateException(Throwable cause) {
        super(cause);
    }
}
