package exam.core.service;

import exam.common.model.Person;
import exam.common.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;


public class PersonServiceClientImpl implements PersonService {

    @Autowired
    private PersonService person_service;

    @Override
    public Person searchPersonBySSN(String ssn) {
        return person_service.searchPersonBySSN(ssn);
    }
}
