package exam.core.model;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "book")
public class Book extends BaseEntity<Long> {

    @Column(name = "ISBN", nullable = false)
    private String isbn;
    @Column(name = "Title", nullable = false)
    private String title;
    @Column(name = "Year", nullable = false)
    private Integer year;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "AuthorID")
    private Author author;

    public Book(Long id) {
        super(id);
    }

    public Book(Long id, String isbn, String title, Integer year) {
        super(id);
        this.isbn = isbn;
        this.title = title;
        this.year = year;
    }
}
