import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import DS from 'ember-data';

export default Model.extend({
  title: attr('string'),
  isbn: attr('string'),
  year: attr('number'),
  //author: DS.belongsTo('author')
});
