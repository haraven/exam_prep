import DS from "ember-data";

export default DS.RESTAdapter.extend({
  namespace: 'ab',
  host: 'http://localhost:13337',
  headers: {
    'Accept': 'application/vnd.api+json',
    'Content-Type': 'application/vnd.api+json'
  }
});
