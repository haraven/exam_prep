import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('authors', function() {
    this.route('new');
  });
  this.route('authorbooks', { path: '/authors/:author_id' });
});

export default Router;
