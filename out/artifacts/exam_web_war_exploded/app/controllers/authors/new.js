import Ember from 'ember';

export default Ember.Controller.extend({
  ssn: null,
  person: null,
  actions: {
    onSearchClicked() {
      var ssn = this.get('ssn');
      this.set('person', this.store.findRecord('person', ssn));
    },
    onAddClicked() {
      var person_name = this.get('person').get('name');
      var author = this.store.createRecord('author',
      {
        id: null,
        name: person_name,
        books: []
      });

      author.save()
      .then(this.transitionToRoute('authors'));
    }
  }
});
