import Ember from 'ember';

export default Ember.Route.extend({
  model(params) {
     return Ember.RSVP.hash({
       books: this.store.query('book', { author_id : params.author_id })
     });
  }
});
