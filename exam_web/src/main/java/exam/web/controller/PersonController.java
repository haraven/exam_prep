package exam.web.controller;

import exam.common.model.Person;
import exam.core.service.PersonServiceClientImpl;
import exam.web.dto.PersonDTO;
import exam.web.utils.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PersonController {

    @Autowired
    private PersonServiceClientImpl person_service;

    @RequestMapping(value = "/people/{ssn}", method = RequestMethod.GET, produces = MediaType.JSON_API)
    public PersonDTO getPerson(@PathVariable("ssn") String ssn) {
        //return new PersonDTO(new Person(11L, ssn, "this is a name"));
        Person person = person_service.searchPersonBySSN(ssn);
        return new PersonDTO(person);
    }
}
