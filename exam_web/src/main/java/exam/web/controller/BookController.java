package exam.web.controller;

import exam.core.model.Book;
import exam.core.service.BookService;
import exam.web.dto.BooksDTO;
import exam.web.utils.MediaType;
import exam.web.utils.converters.BookDTOConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@CrossOrigin
public class BookController {
    @Autowired
    private BookService book_service;
    @Autowired
    private BookDTOConverter book_dto_converter;

    @RequestMapping(value = "/books", method = RequestMethod.GET, produces = MediaType.JSON_API)
    public BooksDTO getAuthorBooks(@RequestParam("author_id") Long author_id) {
        List<Book> books = book_service.getAuthorBooks(author_id);
        Collections.sort(books, (b1, b2) -> b1.getYear() - b2.getYear());
        return new BooksDTO(book_dto_converter.modelsToDTOs(books));
    }
}
