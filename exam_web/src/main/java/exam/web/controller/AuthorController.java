package exam.web.controller;

import exam.core.model.Author;
import exam.core.model.Book;
import exam.core.service.AuthorService;
import exam.web.dto.AuthorDTO;
import exam.web.dto.BooksDTO;
import exam.web.dto.EmptyJSONResponse;
import exam.web.utils.converters.BookDTOConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import exam.web.dto.AuthorsDTO;
import exam.web.utils.MediaType;
import exam.web.utils.converters.AuthorDTOConverter;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class AuthorController {
    @Autowired
    private AuthorService author_service;

    @Autowired
    private AuthorService book_service;

    @Autowired
    private AuthorDTOConverter author_dto_converter;
    @Autowired
    private BookDTOConverter book_dto_converter;

    @RequestMapping(value = "/authors", method = RequestMethod.GET, produces = MediaType.JSON_API)
    public AuthorsDTO getAuthors() {
        List<Author> authors = author_service.getAll();

        return new AuthorsDTO(author_dto_converter.modelsToDTOsWithBooks(authors));
    }

    @RequestMapping(value = "/authors/{author_id}", method = RequestMethod.GET, produces = MediaType.JSON_API)
    public BooksDTO getAuthor(@PathVariable("author_id") Long author_id) {
        List<Book> books = book_service.getAuthorBooks(author_id);
        Collections.sort(books, (b1, b2) -> b1.getYear() - b2.getYear());
        return new BooksDTO(book_dto_converter.modelsToDTOs(books));
    }

    @RequestMapping(value = "/authors", method = RequestMethod.POST, produces = MediaType.JSON_API)
    public EmptyJSONResponse addAuthor(@RequestBody final Map<String, AuthorDTO> author_dto_map) {
        AuthorDTO author = author_dto_map.get("author");
        author_service.addAuthor(author_dto_converter.toModel(author));

        return new EmptyJSONResponse();
    }
}
