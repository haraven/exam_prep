package exam.web.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@ToString
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class AuthorDTO extends BaseDTO<Long> {
    private String name;
    //private List<BookDTO> books = new ArrayList<>();
    private Integer bookCount;

    public AuthorDTO(Long id) {
        super(id);
    }
}
