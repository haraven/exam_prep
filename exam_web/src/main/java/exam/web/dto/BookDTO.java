package exam.web.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class BookDTO extends BaseDTO<Long> {
    private String isbn;
    private String title;
    private int year;
//    private AuthorDTO author;

    public BookDTO(Long id) {
        super(id);
    }
}
