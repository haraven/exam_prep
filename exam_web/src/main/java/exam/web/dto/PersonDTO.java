package exam.web.dto;

import exam.common.model.Person;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
public class PersonDTO implements Serializable {
    private Person person;

    public PersonDTO(Person person) {
        this.person = person;

        if (this.person != null && this.person.getId() == null)
            this.person.setId(-1L);
    }
}
