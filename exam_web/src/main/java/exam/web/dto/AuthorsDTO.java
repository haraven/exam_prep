package exam.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AuthorsDTO {
    private List<AuthorDTO> authors;
}
