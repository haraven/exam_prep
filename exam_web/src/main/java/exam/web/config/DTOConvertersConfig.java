package exam.web.config;

import exam.web.utils.converters.AuthorDTOConverter;
import exam.web.utils.converters.BookDTOConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DTOConvertersConfig {
    @Bean
    public BookDTOConverter bookDTOConverter() { return new BookDTOConverter(); }

    @Bean
    AuthorDTOConverter authorDTOConverter() { return new AuthorDTOConverter(); }
}
