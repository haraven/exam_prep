package exam.web.utils.converters;

import exam.core.model.Book;
import exam.web.dto.BookDTO;

public class BookDTOConverter extends BaseConverter<Book, BookDTO> {
    @Override
    public Book toModel(BookDTO dto) {
        AuthorDTOConverter author_converter = new AuthorDTOConverter();
        return Book.builder().isbn(dto.getIsbn())
                .title(dto.getTitle())
                .year(dto.getYear())
                //.author(author_converter.toModel(dto.getAuthor()))
                .build();
    }

    @Override
    public BookDTO toDTO(Book book) {
        AuthorDTOConverter author_converter = new AuthorDTOConverter();
        BookDTO bookDTO = BookDTO.builder()
                .title(book.getTitle())
                .isbn(book.getIsbn())
                .year(book.getYear())
                //.author(author_converter.toDTO(book.getAuthor()))
                .build();

        bookDTO.setId(book.getId());
        return bookDTO;
    }
}
