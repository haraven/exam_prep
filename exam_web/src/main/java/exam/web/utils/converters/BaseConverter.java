package exam.web.utils.converters;

import exam.core.model.BaseEntity;
import exam.web.dto.BaseDTO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseConverter<Model extends BaseEntity<Long>, DTO extends BaseDTO<Long>> implements Converter<Model, DTO> {
    public List<Long> convertModelsToIDs(List<Model> models) {
        return models.stream()
                .map(BaseEntity::getId)
                .collect(Collectors.toList());
    }

    public List<Long> DTOsToIDs(List<DTO> dtos) {
        return dtos.stream()
                .map(BaseDTO::getId)
                .collect(Collectors.toList());
    }

    public List<DTO> modelsToDTOs(Collection<Model> models) {
        return models.stream()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }
}
