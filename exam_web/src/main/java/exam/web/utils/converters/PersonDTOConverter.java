package exam.web.utils.converters;

import exam.common.model.Person;
import exam.web.dto.PersonDTO;

public class PersonDTOConverter {
    public Person toModel(PersonDTO dto) {
        return dto.getPerson();
    }

    public PersonDTO toDTO(Person person) {
        return new PersonDTO(person);
    }
}
