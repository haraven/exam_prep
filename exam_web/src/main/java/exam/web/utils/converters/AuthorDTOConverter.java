package exam.web.utils.converters;

import exam.core.model.Author;
import exam.core.model.Book;
import exam.web.dto.AuthorDTO;
import exam.web.dto.BookDTO;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class AuthorDTOConverter extends BaseConverter<Author, AuthorDTO> {
    @Autowired
    private BookDTOConverter book_dto_converter;

    @Override
    public Author toModel(AuthorDTO dto) {
        return Author.builder().name(dto.getName()).build();
    }

    @Override
    public AuthorDTO toDTO(Author author) {
        AuthorDTO authorDTO = AuthorDTO.builder().name(author.getName()).bookCount(author.getBooks().size()).build();
        authorDTO.setId(author.getId());

        return authorDTO;
    }

    public AuthorDTO toDTOWithBooks(Author author) {
        AuthorDTO authorDTO = toDTO(author);
        List<BookDTO> book_dtos = new ArrayList<>();
        author.getBooks().forEach(book -> book_dtos.add(book_dto_converter.toDTO(book)));
        authorDTO.setBookCount(book_dtos.size());

        return authorDTO;
    }

    public List<AuthorDTO> modelsToDTOsWithBooks(Collection<Author> authors) {
        return authors.stream()
                .map(this::toDTOWithBooks)
                .collect(Collectors.toList());
    }
}
