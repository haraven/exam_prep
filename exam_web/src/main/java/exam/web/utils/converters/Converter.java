package exam.web.utils.converters;


import exam.core.model.BaseEntity;
import exam.web.dto.BaseDTO;

public interface Converter<Model extends BaseEntity<Long>, DTO extends BaseDTO> {
    Model toModel(DTO dto);
    DTO toDTO(Model model);
}
